# 3.0
* **URL:** https://gitlab.com/gitlab-it/okta-sdk/-/milestones/3
* **Release Date:** 2022-12-05

## Overview

### v2 to v3 Upgrade Guide

#### What's Changed

* The `glamstack/okta-sdk` has been abandoned and has been renamed to `gitlab-it/okta-sdk`.
* The `config/glamstack-gitlab.php` was renamed to `config/gitlab-sdk.php`. No array changes were made.
* The namespace changed from `Glamstack\Okta` to `GitlabIt\Okta`.
* Changed from a modified version of [Calendar Versioning (CalVer)](https://calver.org/) to using [Semantic Versioning (SemVer)](https://semver.org/).
* License changed from `Apache 2.0` to `MIT`

#### Migration Steps

1. Remove `glamstack/okta-sdk` from `composer.json` and add `"gitlab-it/okta-sdk": "^3.0"`, then run `composer update`.
1. Navigate to your `config` directory and rename `glamstack-okta.php` to `okta-sdk.php`.
1. Perform a find and replace across your code base from `Glamstack\Okta` to `GitlabIt\Okta`.
1. Perform a find and replace for `config('glamstack-okta.` to `config('okta-sdk.`

### New Features

* Add `.gitlab-ci.yml` file for GitLab CI pipeline jobs
* Add `gitlab-it/laravel-docker` Docker image as default image to `.gitlab-ci.yml`
* Add Code Quality CI template job to `.gitlab-ci.yml`
* Add Dependency Scanning template job to `.gitlab-ci.yml`
* Add Security/SAST template job to `.gitlab-ci.yml`
* Add Secret Detection template to `gitlab-ci.yml`
* Add PHPCS PSR12 codestyle job to `.gitlab-ci.yml`
* Fix PSR-12 formatting in affected files
* Update `ApiClient::parseApiResponse` to refactor check for `paginated_results` property in response
* Update `CONTRIBUTING.md` with `okta-sdk` and related v3 content updates
* Update `README.md` with new v3 usage instructions

### Breaking Changes

* (High Risk) Changed version publish flag from `glamstack-okta` to `okta-sdk` (`php artisan vendor:publish --tag=okta-sdk`)
* (High Risk) The `config/glamstack-gitlab.php` was renamed to `config/gitlab-sdk.php`. No array changes were made.
* (High Risk) The `glamstack/okta-sdk` has been abandoned and has been renamed to `gitlab-it/okta-sdk`.
* (High Risk) The namespace changed from `Glamstack\Okta` to `GitlabIt\Okta`.
* (High Risk) Updated version constraint recommendation to `composer require gitlab-it/okta-sdk:^3.0`
* (Medium Risk) An error that previously was handled with `abort()` will now throw a new `\Exception`.
* (Medium Risk) Changed from a modified version of [Calendar Versioning (CalVer)](https://calver.org/) to using [Semantic Versioning (SemVer)](https://semver.org/).
* (Medium Risk) Update `ApiClient::handleException` return type from `string` to `object` to normalize API response format with normal response with `error` array and `status` array.
  * Impact: An exception will no longer return a string error message. An object will be returned with `error.code`, `error.message`, `error.reference`, and the standard `status.*` properties. The `status.serverError` will always return `true` for an exception. The `object.*`, `json.*`, and `headers.*` properties will not be returned in an exception response.
* (Low Risk) License changed from `Apache 2.0` to `MIT`
* (Low Risk) Remove `$paginated` optional argument from `ApiClient::parseApiResponse` method
* (Low Risk) Update `ApiClient::delete` change return type from `object|string` to `object`
* (Low Risk) Update `ApiClient::get` change return type from `object|string` to `object`
* (Low Risk) Update `ApiClient::post` change return type from `object|string` to `object`
* (Low Risk) Update `ApiClient::put` change return type from `object|string` to `object`

## Merge Requests (5)

* `backend` Add `.gitlab-ci.yml` file with GitLab templates for code quality and security tests - !18 - @dillonwheeler @jeffersonmartin
* `backend` Add `.gitlab-ci.yml` file with GitLab templates for code quality and security tests - !19 - @jeffersonmartin
* `backend` Fix Okta pagination results to include first 200 results - !16 - @dillonwheeler @jeffersonmartin
* `backend` Fix syntax based on PHPStan results - !7 - @jeffersonmartin
* `backend` v3 Breaking Change: Rename package from glamstack/okta-sdk to gitlab-it/okta-sdk - !17 - @dillonwheeler @jeffersonmartin

## Commits (31)

* `backend` Breaking Change: Rename `Config/glamstack-okta.php` to `Config/okta-sdk.php` - b1136b5b - !17
* `backend` Fix PSR-12 formatting in `src/ApiClient` - 5b488416 - !19
* `backend` Fix PSR-12 formatting in `src/OktaServiceProvider` - b0234783 - !19
* `backend` Fix PSR-12 formatting in `src/Traits/ResponseLog` - cfa3b41e - !19
* `backend` Fix PSR-12 syntax formatting - 0adcccfa - !17
* `backend` Fix `.gitlab-ci.yml` code_style job to change `app` to `src` - 177b57f4 - !19
* `backend` Fix `ApiClient::get` method URL for paginated results to return first 200 results - f02eb5ff - !16
* `backend` Fix `src/ApiClient::put` docblock with return type typo - bf893643 - !7
* `backend` Fix `src/ApiClient` based on PHPStan findings - d440a01d - !7
* `backend` Fix merge conflicts for branch '3.0' into '7-fix-syntax-based-on-phpstan-results' - fd7043bf - !7
* `backend` Rebase `3.0` into `7-fix-syntax-based-on-phpstan` - a5b95c5e - !7
* `backend` Update .gitlab-ci.yml - 7e49b133 - !19
* `backend` Update .gitlab-ci.yml - 894667de - !7
* `backend` Update .gitlab-ci.yml - 9b82bea5 - !18
* `backend` Update `.gitlab-ci.yml` to add `phpstan` static analysis job - c0768d0f - !7
* `backend` Update `CONTRIBUTING.md` to replace `glamstack` with `gitlab-it` - 3965874d - !17
* `backend` Update `CONTRIBUTING.md` to replace `glamstack` with `gitlab-it` - 8f0d4546 - !17
* `backend` Update `CONTRIBUTING.md` with `okta-sdk` and related content updates - e894cba4 - !17
* `backend` Update `composer.json` to rename `glamstack` to `gitlab-it` - 838d21cc - !17
* `backend` Update `src/ApiClient` to rename `glamstack-okta` to `okta-sdk` - 4a7a9ef1 - !17
* `backend` Update `src/OktaServiceProvider` to change namespace from `Glamstack` to GitlabIt` - d425d103 - !17
* `backend` Update `src/OktaServiceProvider` to rename `glamstack-okta` to `okta-sdk` - 293b437f - !17
* `backend` Update `src/Traits/ResponseLog` to change namespace from `Glamstack` to GitlabIt` - fa2ada9f - !17
* `backend` Update src/ApiClient.php docblocks - bf6e6e27 - !17
* `backend` Update src/ApiClient.php to extract common code block in `checkForPagination` method - ca16eaff - !17
* `backend` Update src/ApiClient.php to extract common code block in `validateConnectionConfigArray` method - 5afd3012 - !17
* `backend` Update src/ApiClient.php to fix `generateNextPaginatedResultUrl` return type - 34fbcbce - !16
* `docs` Add `Caching responses` documentation to `README.md` - c4f3d72e - !17
* `docs` Update `composer.json` to change license from `Apache-2.0` to `MIT` - fc3d062c - !17
* `docs` Update `LICENSE` file from `Apache 2.0` to `MIT` - 20324ecb - !17
* `docs` Update `README.md` with `gitlab-it/okta-sdk` and v3 refactored content updates - 72165196 - !17
